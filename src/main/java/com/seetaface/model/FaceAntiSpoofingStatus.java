package com.seetaface.model;

/**
 * @Description 活体识别状态
 * @Author Sugar
 * @Date 2021/2/24 14:49
 */
public enum FaceAntiSpoofingStatus {
    REAL(0),       ///< 真实人脸
    SPOOF(1),      ///< 攻击人脸（假人脸）
    FUZZY(2),      ///< 无法判断（人脸成像质量不好）
    DETECTING(3);  ///< 正在检测

    int status;
    FaceAntiSpoofingStatus(int status) {
        this.status = status;
    }
}
