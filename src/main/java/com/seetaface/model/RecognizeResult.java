package com.seetaface.model;

/**
 * @Author Sugar
 * @Version 2019/4/4 18:07
 */
public class RecognizeResult {
    public int index;
    public float similar;
}
