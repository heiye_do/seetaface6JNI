package com.seetaface.model;

import java.util.Arrays;

public class FaceLandmark extends SeetaRect {
    public SeetaPointF[] points;

    public FaceLandmark() {
    }

    public FaceLandmark(SeetaRect rect) {
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
        this.score = rect.score;
    }

    @Override
    public String toString() {
        return "FaceLandmark{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                ", score=" + score +
                ", points=" + Arrays.toString(points) +
                '}';
    }
}
